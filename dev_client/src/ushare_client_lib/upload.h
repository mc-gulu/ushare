#pragma once

#include <string>

std::string Foo(const std::string& uri);

class PlayerInfo
{
public:
    const std::string& serverName;
    const std::string& userName;
    const std::string& roleName;
};

int QueryTicket(const PlayerInfo& info);

bool IsTicketValid(int ticket);
