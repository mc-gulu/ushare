
#include "stdafx.h"
#include "HttpSession.h"

#include "FileUtility.h"

#include <Poco/StringTokenizer.h>


McConstStr URL_query_ticket = "/query_ticket";
McConstStr URL_validate_files = "/validate_files";
McConstStr URL_upload_resource = "/upload_resource";
McConstStr URL_generate_sharing_link = "/gen_share_link";

const int DIGEST_SIZE =64;

HttpSession::HttpSession()
{
    m_port = 0;
}

bool HttpSession::HasHost()
{
    return !m_host.empty() && m_port != 0;
}

void HttpSession::SetHost(const std::string& host, int port)
{
    m_host = host;
    m_port = port;
}

bool HttpSession::QueryTicket(const std::string& regionName, const std::string& serverName, const std::string& userName, const std::string& roleName)
{
    if (!HasHost())
        return false;

    try
    {
        Poco::Net::HTTPClientSession session(m_host, m_port);
        Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, URL_query_ticket, Poco::Net::HTTPMessage::HTTP_1_1);

        McUniString region(regionName.c_str());
        McUniString server(serverName.c_str());
        McUniString user(userName.c_str());
        McUniString role(roleName.c_str());

        std::string origin = region.ToUtf8() + "|" + server.ToUtf8() + "|" + user.ToUtf8() + "|" + role.ToUtf8(); 

        std::stringstream ss;
        Poco::Base64Encoder b64enc(ss);
        b64enc << origin;
        b64enc.close();

        Poco::Net::HTMLForm form;
        form.add("user_info", ss.str());
        form.prepareSubmit(request);
        form.write(session.sendRequest(request));

        Poco::Net::HTTPResponse res;
        std::istream &is = session.receiveResponse(res);

        std::string digest;
        is >> digest;
        if (digest.size() != DIGEST_SIZE)
            return false;
        
        m_ticket = digest;
    }
    catch (std::exception& e)
    {
        LOG(ERROR) << "Exception Occurred: " << std::endl
            << "  ExFunc:   " << __FUNCTION__ << std::endl
            << "  ExType:   " << typeid(e).name() << std::endl
            << "  ExDetail: " << e.what() << std::endl
            << "  UserInfo: " << "server: " << serverName << "user: " << userName << "role: " << roleName << std::endl;
        return false;
    }

    return true;
}

bool HttpSession::UploadFile(const std::string& filepath)
{
    if (!HasHost())
        return false;

    try
    {
        Poco::Net::HTTPClientSession session(m_host, m_port);
        Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, GenerateURIWithTicket(URL_upload_resource), Poco::Net::HTTPMessage::HTTP_1_1);

        Poco::Net::HTMLForm form;
        form.setEncoding(Poco::Net::HTMLForm::ENCODING_MULTIPART);
        form.addPart("file_list", new Poco::Net::FilePartSource(filepath));
        form.prepareSubmit(request);

        session.setTimeout(Poco::Timespan(20, 0));
        form.write(session.sendRequest(request));        

        Poco::Net::HTTPResponse res;
        std::istream &is = session.receiveResponse(res);
        Poco::StreamCopier::copyStream(is, std::cout);

        LOG(INFO) << "Uploading successfully.";
    }
    catch (std::exception& e)
    {
        LOG(ERROR) << "Exception Occurred: " << std::endl
            << "  ExFunc:   " << __FUNCTION__ << std::endl
            << "  ExType:   " << typeid(e).name() << std::endl
            << "  ExDetail: " << e.what() << std::endl
            << "  UserInfo: " << "uploading: " << filepath << std::endl;
        return false;
    }

    return true;
}

bool HttpSession::ValidateRequestedFiles(const std::map<std::string, std::string>& files, std::vector<FileInfo>& validatedFiles)
{
    if (!HasHost())
        return false;

    std::vector<FileInfo> requestedFiles;
    for (const std::pair<std::string, std::string>& p : files)
    {
        int fileSize = 0;
        std::string fileMD5;
        if (!FileUtility::GetFileInfo(p.first, &fileSize, &fileMD5))
        {
            LOG(ERROR) << "GetFileInfo() failed. ('" << p.first << "')";
            return false;
        }

        requestedFiles.emplace_back(p.first, fileSize, fileMD5);
    }

    try
    {
        Poco::Net::HTTPClientSession session(m_host, m_port);
        Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, GenerateURIWithTicket(URL_validate_files), Poco::Net::HTTPMessage::HTTP_1_1);

        Poco::Net::HTMLForm form;

        for (FileInfo& fi : requestedFiles)
            form.add(fi.m_filePath, McIntToString(fi.m_fileSize) + "|" + fi.m_fileChecksum);

        form.prepareSubmit(request);
        form.write(session.sendRequest(request));

        Poco::Net::HTTPResponse res;
        std::istream &is = session.receiveResponse(res);

        std::string neededFiles;
        is >> neededFiles;

        Poco::StringTokenizer tokenizer(neededFiles, "|", Poco::StringTokenizer::TOK_TRIM);
        for (Poco::StringTokenizer::Iterator it = tokenizer.begin(); it != tokenizer.end(); ++it)
        {
            for (FileInfo& fi : requestedFiles)
            {
                if (fi.m_filePath == *it)
                {
                    validatedFiles.push_back(fi);
                }
            }
        }

        LOG(INFO) << "Validated file count: " << validatedFiles.size();
    }
    catch (std::exception& e)
    {
        LOG(ERROR) << "Exception Occurred: " << std::endl
            << "  ExFunc:   " << __FUNCTION__ << std::endl
            << "  ExType:   " << typeid(e).name() << std::endl
            << "  ExDetail: " << e.what() << std::endl;
        return false;
    }

    return true;
}

bool HttpSession::GenerateSharedURL(const std::map<std::string, std::string>& files, const std::string& playerWords)
{
    if (!HasHost())
        return false;

    try
    {
        Poco::Net::HTTPClientSession session(m_host, m_port);
        Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, GenerateURIWithTicket(URL_generate_sharing_link), Poco::Net::HTTPMessage::HTTP_1_1);

        Poco::Net::HTMLForm form;

        for (const std::pair<std::string, std::string>& file : files)
        {
            form.add("file", file.first + "|" + file.second);
        }

        form.add("player_words", playerWords);

        form.prepareSubmit(request);
        form.write(session.sendRequest(request));

        Poco::Net::HTTPResponse res;
        std::istream &is = session.receiveResponse(res);

        std::string generatedLinkInfo;
        is >> generatedLinkInfo;
        if (generatedLinkInfo.size())
        {
            m_shareID = generatedLinkInfo;
            LOG(INFO) << "share_id: " << m_shareID;
        }
        else
        {
            LOG(INFO) << "share_id generated failed.";
        }
    }
    catch (std::exception& e)
    {
        LOG(ERROR) << "Exception Occurred: " << std::endl
            << "  ExFunc:   " << __FUNCTION__ << std::endl
            << "  ExType:   " << typeid(e).name() << std::endl
            << "  ExDetail: " << e.what() << std::endl;
        return false;
    }

    return true;
}

std::string HttpSession::GenerateURIWithTicket(const std::string& rawRequest)
{
    return tfm::format("%s?ticket=%s", std::string(rawRequest), m_ticket);
}



