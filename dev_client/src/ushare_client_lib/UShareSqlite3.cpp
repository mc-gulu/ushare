/*!
 * \file KSO3SharingSqlite3.cpp
 * \date 2014/12/02 10:36
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Implementation of KSO3SharingSqlite3
*/

#include "stdafx.h"
#include "UShareSqlite3.h"

#include <sqlite3/sqlite3.h>

const char* GSessionTableName = "sessions";
const char* GSessionTableFields = "RegionName text, ServerName text, AccountName text, RoleName text, StartedTime text, UniqueID text";

const char* GRecordTableName = "records";
const char* GRecordTableFields = "SessionID text, FilePath text, Description text";

const DWORD KSHARING_SQL_LEN = 512;

class KSO3Sql
{
public:
    static std::string Format_IfTableExist(const char* tableName) 
    {
        char buf[KSHARING_SQL_LEN];
        sprintf_s(buf, "SELECT count(*) FROM sqlite_master WHERE type='table' AND name='%s';", tableName);
        return buf;
    }

    static std::string Format_CreateTable(const char* tableName, const char* tableFields)
    {
        char buf[KSHARING_SQL_LEN];
        sprintf_s(buf, "CREATE TABLE %s(%s);", tableName, tableFields);
        return buf;
    }

    static std::string Format_InsertRecord(const char* tableName, const char* recordValues)
    {
        char buf[KSHARING_SQL_LEN];
        sprintf_s(buf, "INSERT INTO %s VALUES(%s);", tableName, recordValues);
        return buf;
    }
};



KSO3SharingSqlite3ReturnedData GSqliteReturnedData;

static int KSO3Sqlite3TypicalCallback(void *NotUsed, int argc, char **argv, char **azColName)
{
    if (argc > 0)
    {
        if (!GSqliteReturnedData.Add(argc, argv))
        {
            LOG(INFO) << "DbErr: Failed to add result to GSqliteReturnedData (detailed info follows).";
            for(int i=0; i<argc; i++)
                LOG(INFO) <<"  " << i << ": " << azColName[i] << " = " << argv[i] << ".";
        }
        //else
        //{
        //    LOG(INFO) << "DbInfo: added (detailed info follows).";
        //    for(int i=0; i<argc; i++)
        //        LOG(INFO) <<"  " << i << ": " << azColName[i] << " = " << argv[i] << ".";
        //}
    }
    return 0;
}

UShareSqlite3::~UShareSqlite3()
{
    Reset();
}

bool UShareSqlite3::Open(const char* filename)
{
    if (m_sqlite)
    {
        LOG(INFO) << "DbWarn: Trying to open " << filename << " while db is already connected.";
        return true;
    }

    int ret = sqlite3_open(filename, &m_sqlite);
    if (ret)
    {
        LOG(INFO) << "DbErr: Can't open database: " << sqlite3_errmsg(m_sqlite);
        Reset();
        return false;
    }

    return true;
}

void UShareSqlite3::Reset()
{
    if (m_sqlite)
    {
        sqlite3_close(m_sqlite);
        m_sqlite = nullptr;
    }
}

bool UShareSqlite3::ExecuteSql(const std::string& sql, KSO3SharingSqlite3ReturnedData* ret)
{
    if (sql.empty())
    {
        LOG(INFO) << "DbErr(ExecSql): Empty sql statement.\n";
        return false;
    }

    GSqliteReturnedData.Reset();
    char *errMsg = 0;
    int rc = sqlite3_exec(m_sqlite, sql.c_str(), KSO3Sqlite3TypicalCallback, 0, &errMsg);
    if( rc != SQLITE_OK )
    {
        LOG(INFO) 
            << "DbErr(ExecSql): " << errMsg << std::endl
            << "  Exec: " << sql;
        sqlite3_free(errMsg);
        return false;
    }

    if (ret)
    {
        ret->Reset();
        *ret = GSqliteReturnedData;
        ret->m_sql = sql;
    }

    GSqliteReturnedData.Reset();
    return true;
}

bool KSO3SharingSqlite3ReturnedData::Add(int argc, char **argv)
{
    char buf[KSHARING_SQL_LEN];
    memset(buf, 0, KSHARING_SQL_LEN);
    for(int i=0; i<argc; i++)
    {
        if (i == 0)
        {
            if (strcpy_s(buf, KSHARING_SQL_LEN, argv[i] ? argv[i] : "NULL") != 0)
                return false;
        } 
        else
        {
            if (strcat_s(buf, KSHARING_SQL_LEN, argv[i] ? argv[i] : "NULL") != 0)
                return false;
        }

        if (i != argc - 1)
        {
            if (strcat_s(buf, "|") != 0)
                return false;
        }
    }
    m_values.push_back(buf);
    return true;
}

bool KSO3SharingSqlite3ReturnedData::ParseAsCount(int* pRetCount)
{
    if (m_values.size() != 1)
        return false;

    try
    {
        *pRetCount = std::stoi(m_values[0]);
    }
    catch (std::exception& /*e*/) // including 'std::invalid_argument' & 'std::out_of_range'
    {
        return false;
    }

    return true;
}

void KSO3SharingSqlite3ReturnedData::Dump()
{
    LOG(INFO) <<"DbInfo: SQL executed successfully, " << m_values.size() << " result(s) returned.";
    LOG(INFO) <<"  --SQL-STATEMENT--";
    LOG(INFO) <<"    " << m_sql;
    LOG(INFO) <<"  --SQL-RESULT-BEGIN--------------------";
    for(size_t i = 0; i < m_values.size(); i++)
        LOG(INFO) <<"    " << i << " : " << m_values[i];
    LOG(INFO) <<"  --SQL-RESULT-END----------------------\n";
}
