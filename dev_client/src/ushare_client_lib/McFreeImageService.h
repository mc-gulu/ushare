/*!
 * \file McFreeImageService.h
 * \date 2014/12/15 15:40
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Definition of McFreeImageService 
*/

#pragma once

#include <memory>
#include <functional>

struct FIBITMAP;

template <typename T>
class McImageDeleter
{
public:
    void operator() (T* dib) const = delete;
};

template<>
class McImageDeleter<FIBITMAP>
{
public:
    McImageDeleter() {}
    McImageDeleter(std::function<void (FIBITMAP* dib)> deleter) : m_deleter(deleter) {}
    void operator() (FIBITMAP* dib) const { if (m_deleter && dib) { m_deleter(dib); } }
    std::function<void (FIBITMAP* dib)> m_deleter;
};

typedef std::unique_ptr<FIBITMAP, McImageDeleter<FIBITMAP>> scopedBitmap_t;

/*
    McFreeImageService

    一个 FreeImage 的服务类
    此头文件不依赖 FreeImage.h
 */

class McFreeImageService
{
public:
    McFreeImageService();
    ~McFreeImageService();

    // 根据路径加载指定的图片文件
    FIBITMAP* LoadImage(const char* filename);
    // 规整化图片
    bool NormalizeImage(FIBITMAP* dib, const char* destFilename, int targetWidth, int targetHeight);
    
    // 直接销毁该 Image 释放对应的内存 (使用引用，这样该指针会被置空)
    void ReleaseImage(FIBITMAP*& dib);
    // 使用 scopedBitmap_t 可以在出作用域时自动释放 FIBITMAP 的资源。
    scopedBitmap_t MakeScoped(FIBITMAP* dib);

    // 获取尺寸信息
    bool GetSize(FIBITMAP* dib, int* width, int* height);

    // 错误处理：如果外界需要获取详细的错误信息，需要设置 s_errLogger。
    static std::ostream* s_errLogger;
};

