#include "stdafx.h"
#include "FileUtility.h"

#include <Poco/DigestEngine.h>
#include <Poco/DigestStream.h>
#include <Poco/MD5Engine.h>

#include <fstream>

bool FileUtility::GetFileInfo(const std::string& filepath, int* fileSize, std::string* md5checksum)
{
    MC_ASSERT_RET_BOOL(!filepath.empty());
    MC_ASSERT_RET_BOOL(fileSize);
    MC_ASSERT_RET_BOOL(md5checksum);

    // check if the target file exists, and get its size 
    FILE* f = nullptr;
    errno_t err = fopen_s(&f, filepath.c_str(), "rb");
    if (err != 0 || !f)
        return false;

    fseek(f, 0, SEEK_END);
    *fileSize = ftell(f);
    rewind(f);

    char* buf = new char[*fileSize];
    fread_s(buf, *fileSize, 1, *fileSize, f);
    fclose(f);

    // compute md5
    Poco::MD5Engine md5;
    md5.update(buf, *fileSize);
    *md5checksum = Poco::DigestEngine::digestToHex(md5.digest());

    delete []buf;

    return true;
}


void GetTimeString(std::string* dateStr, std::string* timeStr)
{
    if (!dateStr || !timeStr)
        return;

    time_t rawtime;
    struct tm timeinfo;
    char buffer[80];

    time (&rawtime);
    localtime_s(&timeinfo, &rawtime);

    strftime(buffer,80,"%Y-%m-%d", &timeinfo);
    *dateStr = buffer;

    strftime(buffer,80,"%H-%M-%S", &timeinfo);
    *timeStr = buffer;
}

BOOL DirectoryExistsA(LPCSTR szPath)
{
    DWORD dwAttrib = GetFileAttributesA(szPath);
    return (dwAttrib != INVALID_FILE_ATTRIBUTES && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

BOOL PrepareDirectoryA(LPCSTR szPath)
{
    if (!DirectoryExistsA(szPath))
        if (!CreateDirectoryA(szPath, NULL))
            return FALSE;

    return TRUE;
}

bool InitEnv(const char* appName, const char* tempDir)
{
    std::string dateStr;
    std::string timeStr;
    GetTimeString(&dateStr, &timeStr);

    if (!PrepareDirectoryA(tempDir))
    {
        printf("initEnv() error - (GTempDir not prepared)");
        return false;
    }

    std::string logDir = tempDir + dateStr + "/";
    if (!PrepareDirectoryA(logDir.c_str()))
    {
        printf("initEnv() error - (logDir '%s' preparing failed)", logDir.c_str());
        return false;
    }

    std::string qrcodeDir = std::string(tempDir) + QRCODE_DIR + "/";
    if (!PrepareDirectoryA(qrcodeDir.c_str()))
    {
        printf("initEnv() error - (qrcodeDir '%s' preparing failed)", qrcodeDir.c_str());
        return false;
    }

    el::Configurations conf("logging.conf");
    std::string logFile = logDir + timeStr + "-" + appName + ".log";
    conf.set(el::Level::Global, el::ConfigurationType::Filename, logFile.c_str());
    el::Loggers::reconfigureAllLoggers(conf);

    LOG(INFO) << "InitEnv() done." << std::endl
        << "  File: " << logFile;
    return true;
}

HANDLE RunProgram(const char* appName, char* appArgs /*= ""*/)
{
    STARTUPINFOA si = {sizeof(si)} ;
    PROCESS_INFORMATION pi ;
    if (!CreateProcessA(appName, appArgs, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
    {
        LOG(ERROR) << "CreateProcessA() failed. (" << appName << " " << appArgs << ")";
        return NULL;
    }
    return pi.hProcess;
}
