/*!
 * \file UShareLua51.h
 * \date 2014/12/06 15:44
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Definition of UShareLua51 
*/

#pragma once

const int LUA_INVALID_INT = 0x80000001;

/*
 	UShareLua51 : Lua 环境的封装 

    ===== Remarks ===== 

    这个头文件不依赖 lua.h 和 LuaBridge.h 
    目的是避免业务逻辑对 lua 环境头文件有不必要的物理依赖
 */

struct lua_State;

class UShareLua51 
{
    MC_SINGLETON_DEF(UShareLua51);

public:
    bool Init(const char* configScript);

    std::string     GetGlobalString(const char* name);
    int             GetGlobalInt(const char* name);

private:
    lua_State* LuaState;
};

