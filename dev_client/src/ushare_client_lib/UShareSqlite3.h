/*!
 * \file UShareSqlite3.h
 * \date 2014/12/05 17:10
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Definition of UShareSqlite3 
*/

#pragma once

/*
 	UShareSqlite3 : sqlite 环境的封装 

    ===== Remarks ===== 

    这个头文件不依赖 sqlite3.h 
    目的是避免业务逻辑对 sqlite 环境头文件有不必要的物理依赖
 */

struct sqlite3;
class KSO3SharingSqlite3ReturnedData;

class UShareSqlite3
{
public:
    UShareSqlite3() : m_sqlite(nullptr) {}
    ~UShareSqlite3();

    bool Open(const char* filename);
    void Reset();

    bool ExecuteSql(const std::string& sql, KSO3SharingSqlite3ReturnedData* ret);

private:
    sqlite3* m_sqlite;
};

class KSO3SharingSqlite3ReturnedData
{
public:
    std::string m_sql;
    std::vector<std::string> m_values;

    bool Add(int argc, char **argv);
    bool ParseAsCount(int* pRetCount);
    void Reset() { m_values.clear(); }
    void Dump();
};

