
#include "stdafx.h"
#include "upload.h"

#include <Poco/StreamCopier.h>
#include <Poco/Net/FilePartSource.h>
#include <Poco/Net/Net.h>
#include <Poco/Net/HTMLForm.h>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>

#include <sstream>
#include <Poco/Base64Encoder.h>
#include <Poco/Base64Decoder.h>

using namespace Poco;
using namespace Poco::Net;

// http://stackoverflow.com/questions/11914388/sending-an-image-using-poco-to-a-webserver?rq=1
// http://stackoverflow.com/questions/13968134/poco-how-to-upload-image-to-webser-using-poco-in-c


std::string Foo(const std::string& uri)
{
    HTTPRequest request(HTTPRequest::HTTP_POST, "/upload_resource",    HTTPMessage::HTTP_1_1);
    HTMLForm form;
    form.setEncoding(HTMLForm::ENCODING_MULTIPART);
    form.addPart("myfiles", new FilePartSource("../../test/images/os-android.png"));
    //form.addPart("myfiles", new FilePartSource("../../test/images/os-ios.png"));
    form.prepareSubmit(request);

    HTTPClientSession *httpSession = new HTTPClientSession("localhost", 13080);
    httpSession->setTimeout(Timespan(20, 0));
    form.write(httpSession->sendRequest(request));        

    Net::HTTPResponse res;
    std::istream &is = httpSession->receiveResponse(res);
    StreamCopier::copyStream(is, std::cout);

    return "";
}
