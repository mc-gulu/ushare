#pragma once


template <typename ELEM, typename CONT>
bool UIsInContainer(ELEM e, CONT c) 
{
    return std::find(c.begin(), c.end(), e) != c.end(); 
}



void GetTimeString(std::string* dateStr, std::string* timeStr);

BOOL DirectoryExistsA(LPCSTR szPath);

BOOL PrepareDirectoryA(LPCSTR szPath);                                   

bool InitEnv(const char* appName, const char* tempDir);

const char* const QRCODE_DIR = "qrcodes";

class FileInfo
{
public:
    std::string m_filePath;
    int m_fileSize;
    std::string m_fileChecksum;

    FileInfo(): m_fileSize(0) {}

    FileInfo(const std::string& filepath, int fileSize, const std::string& md5checksum)
        : m_filePath(filepath)
        , m_fileSize(fileSize)
        , m_fileChecksum(md5checksum)
    {}
};

class FileUtility
{
public:
    static bool GetFileInfo(const std::string& filepath, int* fileSize, std::string* md5checksum);
};

HANDLE RunProgram(const char* appName, char* appArgs = "");
