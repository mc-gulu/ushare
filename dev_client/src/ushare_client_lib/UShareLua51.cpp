/*!
 * \file UShareLua51.cpp
 * \date 2014/12/06 15:44
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Implementation of UShareLua51
*/

#include "stdafx.h"
#include "UShareLua51.h"


extern "C" {
# include "lua.h"
# include "lauxlib.h"
# include "lualib.h"
}

#pragma comment (lib, "lua5.1.lib")

#include <LuaBridge.h>


MC_SINGLETON_IMPL(UShareLua51);

UShareLua51::UShareLua51() 
    : LuaState(nullptr)
{
}

UShareLua51::~UShareLua51()
{
    lua_close(LuaState);
}

bool UShareLua51::Init(const char* configScript)
{
    LuaState = luaL_newstate();
    luaL_openlibs(LuaState);
    luaL_dofile(LuaState, configScript);

    return true;
}

std::string UShareLua51::GetGlobalString(const char* name)
{
    luabridge::LuaRef r = luabridge::getGlobal(LuaState, name);
    if (r.isString())
        return r.cast<std::string>();

    return "";
}

int UShareLua51::GetGlobalInt(const char* name)
{
    luabridge::LuaRef r = luabridge::getGlobal(LuaState, name);
    if (r.isNumber())
        return r.cast<int>();

    return LUA_INVALID_INT;
}
