/*!
 * \file PlayerDataCenter.cpp
 * \date 2014/12/05 18:38
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Implementation of PlayerDataCenter
*/

#include "stdafx.h"
#include "PlayerDataCenter.h"

#include "FileUtility.h"
#include "UShareSqlite3.h"

#include "McFreeImageService.h"

#include <Poco/StringTokenizer.h>

#include <algorithm>

bool PlayerDataCenter::Init(const char* database)
{
    m_freeimage = new McFreeImageService;

    m_sqlite = new UShareSqlite3;
    if (!m_sqlite->Open(database))
        return false;
    LOG(INFO) << "Connected to the sharing database.";

    KSO3SharingSqlite3ReturnedData ret;
    if (!m_sqlite->ExecuteSql("select * from records", &ret))
        return false;
    LOG(INFO) << ret.m_values.size() << " records are readed from the sharing database.";

    int seqID = 0;
    for (std::string& val: ret.m_values)
    {
        Poco::StringTokenizer tokenizer(val, "|", Poco::StringTokenizer::TOK_TRIM);
        if (tokenizer.count() != 3)
        {
            LOG(ERROR) << "Invalid entry: " << val;
            continue;
        }

        UploadingRecord record(++seqID, tokenizer[0], tokenizer[1], tokenizer[2]);
        std::string file = "..\\" + record.m_filePath;
        FILE* f = nullptr;
        errno_t err = fopen_s(&f, file.c_str(), "rb");
        bool exists = err == 0 && f;
        if (f)
            fclose(f);
        if (!exists)
        {
            LOG(ERROR) << "Invalid entry: " << val;
            continue;
        }

        m_records.push_back(record);
    }
    LOG(INFO) << m_records.size() << " records are added to the data center.";

    return true;
}

static const int EXPECTED_WIDTH = 1024;
static const int EXPECTED_HEIGHT = 768;

bool PlayerDataCenter::NormalizeImages()
{
    if (!m_freeimage)
        return false;

    std::vector<std::string> failed;

    LOG(INFO) << "Starting the image normalizing process:";
    for (UploadingRecord& record : m_records)
    {
        std::string file = "..\\" + record.m_filePath;

        FIBITMAP* dib = m_freeimage->LoadImage(file.c_str());
        if (dib)
        {
            if (!m_freeimage->NormalizeImage(dib, file.c_str(), EXPECTED_WIDTH, EXPECTED_HEIGHT))
            {
                failed.push_back(file);
            }
        }
        m_freeimage->ReleaseImage(dib);
    }

    if (failed.size() > 0)
    {
        LOG(ERROR) << "Errors occurred in image normalizing process, details are listed below. (Count: " << failed.size() << ")" << std::endl;
        for (std::string& f : failed)
        {
            LOG(INFO) << "  " << f << std::endl;
        }
        return false;
    }
    else
    {
        LOG(INFO) << "Image normalizing process done.";
    }

    return true;
}

std::string PlayerDataCenter::GenImagesInfoJson()
{
    bool first = true;
    std::string json = "{ \"images\": [";
    for (UploadingRecord& record : m_records)
    {
        if (first)
        {
            first = false;
        }
        else
        {
            json += ",\n";
        }

        std::string path = record.m_filePath;
        std::replace(path.begin(), path.end(), '\\', '/');

        PlayerInfo info;
        if (QueryPlayerInfo(record.m_sessionID, &info))
        {
            json += tfm::format("{ \"id\": %d, \"name\": \"%s\", \"path\": \"/%s\", \"role_info\": \"/%s\"}", record.m_seqID, record.m_desc, path, info.Build());
        }
    }
    json += "]}";

    return json;
}

UploadingSession PlayerDataCenter::ComposeUploadingSession(const std::vector<int>& shareList)
{
    UploadingSession targetSession;

    for (UploadingRecord& record : m_records)
    {
        // 未选中
        if (!UIsInContainer(record.m_seqID, shareList))
            continue;

        if (targetSession.m_sessionID.empty())
            targetSession.m_sessionID = record.m_sessionID;
        else if (targetSession.m_sessionID != record.m_sessionID)
        {
            LOG(ERROR) << "[flash逻辑问题] 选中了这张图，但是跟第一张图不在同一个 session: " << record.m_filePath;
            continue; 
        }

        targetSession.m_selectedRecords.push_back(record);
    }

    // 没有选中合适的图片，返回无效的 session
    if (targetSession.m_selectedRecords.size() == 0)
    {
        LOG(ERROR) << "Invalid session: No valid image picked.";
        return UploadingSession();
    }

    if (!QueryPlayerInfo(targetSession.m_sessionID, &targetSession.m_playerInfo))
    {
        LOG(ERROR) << "QueryPlayerInfo() failed.";
        return UploadingSession();
    }

    return targetSession;
}

PlayerDataCenter::~PlayerDataCenter()
{
    if (m_sqlite)
    {
        m_sqlite->Reset();
        McDeletePointer(m_sqlite);
    }

    McDeletePointer(m_freeimage);
}

bool PlayerDataCenter::QueryPlayerInfo(const std::string& sessionID, PlayerInfo* playerInfo)
{
    if (!playerInfo)
        return false;

    std::string sql = tfm::format("select RegionName,ServerName,AccountName,RoleName from sessions where UniqueID='%s'", sessionID);
    KSO3SharingSqlite3ReturnedData ret;
    if (!m_sqlite->ExecuteSql(sql, &ret))
    {
        LOG(ERROR) << "Invalid session: Querying palyer database failed.";
        return false;
    }

    if (ret.m_values.size() != 1)
    {
        LOG(ERROR) << "Invalid session: Querying palyer database failed. (1 return value expected, " << ret.m_values.size() << " return value(s) met.";
        return false;
    }

    Poco::StringTokenizer tokenizer(ret.m_values[0], "|", Poco::StringTokenizer::TOK_TRIM);
    if (tokenizer.count() != 4)
    {
        LOG(ERROR) << "Invalid entry: " << ret.m_values[0];
        return false;
    }

    playerInfo->m_regionName = tokenizer[0];
    playerInfo->m_serverName = tokenizer[1];
    playerInfo->m_accountName = tokenizer[2];
    playerInfo->m_roleName = tokenizer[3];
    return true;
}

