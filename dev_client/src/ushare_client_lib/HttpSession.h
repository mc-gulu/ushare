#pragma once

#include "FileUtility.h"

class HttpSession
{
public:
    HttpSession();

    // session management
    void SetHost(const std::string& host, int port);
    bool HasHost();
    bool QueryTicket(const std::string& regionName, const std::string& serverName, const std::string& userName, const std::string& roleName);
    std::string GetTicket() const { return m_ticket; }

    // uploading
    bool ValidateRequestedFiles(const std::map<std::string, std::string>& files, std::vector<FileInfo>& validatedFiles);
    bool UploadFile(const std::string& filepath);

    // sharing
    bool GenerateSharedURL(const std::map<std::string, std::string>& files, const std::string& playerWords);
    std::string GetShareID() const { return m_shareID; }
    std::string GetSharedURLQRCode() const { return m_sharedURLQRCode; }


private:
    // session info
    std::string m_host;
    int m_port;
    std::string m_ticket;

    //// uploading process
    //std::vector<std::string> m_requestedFiles;
    //std::vector<std::string> m_actuallyNeededFiles;
    //std::vector<FileInfo> m_requestedFiles;

    // sharing process
    std::string m_shareID;
    std::string m_sharedURLQRCode;

    std::string GenerateURIWithTicket(const std::string& rawRequest);
};
