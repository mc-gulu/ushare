/*!
 * \file PlayerDataCenter.h
 * \date 2014/12/05 18:38
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Definition of PlayerDataCenter 
*/

#pragma once

class PlayerInfo
{
public:
    std::string m_regionName;
    std::string m_serverName;
    std::string m_accountName;
    std::string m_roleName;

    bool IsInvalid() 
    {
        return m_regionName.empty() || m_serverName.empty() || m_accountName.empty() || m_roleName.empty();
    }

    std::string Build()
    {
        if (IsInvalid())
            return "";

        return m_regionName + "|" + m_serverName + "|" + m_accountName + "|" + m_roleName;
    }
};

class UploadingRecord
{
public:
    int m_seqID;
    std::string m_sessionID;
    std::string m_filePath;
    std::string m_desc;

    UploadingRecord(int seqID, const std::string& sessionID, const std::string& filePath, const std::string& desc)
        : m_seqID(seqID)
        , m_sessionID(sessionID)
        , m_filePath(filePath)
        , m_desc(desc)
    {
    }
};

class UploadingSession
{
public:
    PlayerInfo m_playerInfo;
    std::string m_sessionID;
    std::vector<UploadingRecord> m_selectedRecords;
};

class McFreeImageService;
class UShareSqlite3;

class PlayerDataCenter 
{
public: 
    PlayerDataCenter() : m_freeimage(nullptr), m_sqlite(nullptr) {}
    ~PlayerDataCenter();

    // 初始化所有展示的图片
    bool Init(const char* database);
    // 处理那些没有经过规整化的图片
    bool NormalizeImages();
    // 生成包含图片信息的 json 给 flash
    std::string GenImagesInfoJson();
    // 生成上传 session 列表
    UploadingSession ComposeUploadingSession(const std::vector<int>& shareList);
    // 根据 session id 查询玩家信息
    bool QueryPlayerInfo(const std::string& sessionID, PlayerInfo* playerInfo);

private:
    McFreeImageService* m_freeimage;
    UShareSqlite3* m_sqlite;
    std::vector<UploadingRecord> m_records;
};

