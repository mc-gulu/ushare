/*!
 * \file McFreeImageService.cpp
 * \date 2014/12/15 15:42
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Implementation of McFreeImageService
*/

#include "stdafx.h"
#include "McFreeImageService.h"

#include <FreeImage.h>
#include <string>

using namespace std::placeholders;

void FreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char *message)
{
    if (McFreeImageService::s_errLogger)
    {
        std::ostream& err = *McFreeImageService::s_errLogger;
        err << "\n*** " 
            << ((fif != FIF_UNKNOWN) ? std::string("Format: ") + FreeImage_GetFormatFromFIF(fif) : "")
            << " Format\n"
            << message 
            << " ***\n" ;
    }
}

std::ostream* McFreeImageService::s_errLogger = nullptr;

McFreeImageService::McFreeImageService()
{
#ifdef FREEIMAGE_LIB
    FreeImage_Initialise();
#endif
    FreeImage_SetOutputMessage(FreeImageErrorHandler);
}

McFreeImageService::~McFreeImageService()
{
#ifdef FREEIMAGE_LIB
    FreeImage_DeInitialise();
#endif
}

FIBITMAP* McFreeImageService::LoadImage(const char* filename)
{
    // deducing type
    FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
    fif = FreeImage_GetFileType(filename, 0);
    if(fif == FIF_UNKNOWN) 
        fif = FreeImage_GetFIFFromFilename(filename);
    if(fif == FIF_UNKNOWN)
        return NULL;

    // valid to load
    if(!FreeImage_FIFSupportsReading(fif))
        return NULL;

    FIBITMAP *dib = FreeImage_Load(fif, filename);
    return dib;
}

scopedBitmap_t McFreeImageService::MakeScoped(FIBITMAP* dib)
{
    auto deleter = std::bind(&McFreeImageService::ReleaseImage, this, _1); 
    return scopedBitmap_t(dib, McImageDeleter<FIBITMAP>(deleter));
}

bool McFreeImageService::NormalizeImage(FIBITMAP* dib, const char* destFilename, int targetWidth, int targetHeight)
{
    if (!dib)
        return false;

    int srcWidth = 0;
    int srcHeight = 0;
    if (!GetSize(dib, &srcWidth, &srcHeight))
        return false;

    // this image has been normalized
    if (srcWidth == targetWidth && srcHeight == targetHeight)
        return true;

    int destWidth = targetWidth;
    int destHeight = targetHeight;
    int destX = 0;
    int destY = 0;
    float srcRatio = (float)srcWidth / (float)srcHeight;
    if (srcRatio > (float)targetWidth / (float)targetHeight)
    {
        destHeight = (int)((float)targetWidth / srcRatio);
        destY = (targetHeight - destHeight) / 2;
    }
    else
    {
        destWidth = (int)((float)targetHeight * srcRatio);
        destX = (targetWidth - destWidth) / 2;
    }

    scopedBitmap_t normBox = MakeScoped(FreeImage_Rescale(dib, destWidth, destHeight, FILTER_CATMULLROM));
    if (!normBox)
        return false;

    scopedBitmap_t target = MakeScoped(FreeImage_Allocate(targetWidth, targetHeight, 24)); // 使用 24 bits 因为 JPEG 的保存只支持 24 bits 或 8 bits
    if (!target)
        return false;

    if (!FreeImage_Paste(target.get(), normBox.get(), destX, destY, 256))
        return false;

    if (!FreeImage_Save(FIF_JPEG, target.get(), destFilename))
        return false;

    return true;
}

bool McFreeImageService::GetSize(FIBITMAP* dib, int* width, int* height)
{
    if (!dib || !width || !height)
        return false;

    int srcWidth = FreeImage_GetWidth(dib);
    int srcHeight = FreeImage_GetHeight(dib);
    if (srcWidth == 0 || srcHeight == 0)
        return false;

    *width = srcWidth;
    *height = srcHeight;
    return true;
}

void McFreeImageService::ReleaseImage(FIBITMAP*& dib)
{
    if (dib)
    {
        FreeImage_Unload(dib);
        dib = NULL;
    }
}
