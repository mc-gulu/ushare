#pragma once

// 全局共享的对 mcr 的依赖

#include "mcr/core/McCoreDef.h"
#include "mcr/core/McString.h"
#include "mcr/core/McCoreAssert.h"
#include "mcr/core/McCoreStrUtil.h"
#include "mcr/core/McCoreGeneric.h"
#include "mcr/core/McCoreClassUtil.h"

#ifdef _DEBUG
#pragma comment (lib, "mcr_static_d")
#else
#pragma comment (lib, "mcr_static")
#endif


#define _ELPP_DISABLE_DEFAULT_CRASH_HANDLING
#include <easylogging++.h>
