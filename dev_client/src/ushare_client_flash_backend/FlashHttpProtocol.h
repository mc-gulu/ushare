#pragma once

#include "../shared_headers.h"

const int SERVER_PORT = 16080;

McConstStr GET_images_info = "/get_images_info";
McConstStr POST_gen_qrcode = "/gen_qrcode";
McConstStr GET_status_info = "/get_status_info";
McConstStr POST_quit = "/quit";


const char FLASH_ARG_DELIMITER = '&';
McConstStr ARGNAME_SharingList = "idArr";
McConstStr ARGNAME_SharingWords = "infotxt";

// 这个函数按照 C++/Flash 的约定，从 flash 传过来的字符串中解出需要的信息
//  srcInfo 是 flash 传过来的字符串 
//          标准的格式为 “ idArr=1,2,3&infotxt=分享文字 ” 
//          其中分享文字部分使用 utf8 编码
bool ExtractPlayerSharingListAndWords(const std::string& srcInfo, std::vector<int>& shareList, McUniString& shareWords);

                                        