#pragma once

using namespace Poco::Net;
using namespace Poco::Util;
using namespace std;


class FlashRequestHandler : public HTTPRequestHandler
{
public:
    virtual void handleRequest(HTTPServerRequest &req, HTTPServerResponse &resp)
    {
        resp.setStatus(HTTPResponse::HTTP_OK);
        resp.setContentType("text/html");

        ostream& out = resp.send();
        out << "<h1>Hello world!</h1>"
            << "<p>Count: "  << ++count         << "</p>"
            << "<p>Host: "   << req.getHost()   << "</p>"
            << "<p>Method: " << req.getMethod() << "</p>"
            << "<p>URI: "    << req.getURI()    << "</p>";
        out.flush();

        cout << endl
            << "Response sent for count=" << count
            << " and URI=" << req.getURI() << endl;
    }

private:
    static int count;
};

class FlashRequestHandlerFactory : public HTTPRequestHandlerFactory
{
public:
    virtual HTTPRequestHandler* createRequestHandler(const HTTPServerRequest & request);
};

class FlashBackendServer : public ServerApplication
{
protected:
    int main(const vector<string> &);
};

