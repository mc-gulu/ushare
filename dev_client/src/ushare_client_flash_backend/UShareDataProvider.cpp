/*!
 * \file UShareDataProvider.cpp
 * \date 2014/12/06 13:57
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Implementation of UShareDataProvider
*/

#include "stdafx.h"
#include "UShareDataProvider.h"


MC_SINGLETON_IMPL(UShareDataProvider);

UShareDataProvider::UShareDataProvider() 
{

}

UShareDataProvider::~UShareDataProvider()
{

}

bool UShareDataProvider::Init(const char* database)
{
    // 读入玩家的分享数据库
    if (!m_dataCenter.Init(database))
    {
        LOG(ERROR) << "Init Player Data Center failed.";
        return false;
    }

    // 规整化那些还没有被规整化的新图片
    if (!m_dataCenter.NormalizeImages())
    {
        LOG(ERROR) << "Normalizing images failed.";
        return false;
    }
    
    // 生成图片信息 Json 给 flash 前端
    std::string json = m_dataCenter.GenImagesInfoJson();
    if (json.empty())
    {
        LOG(ERROR) << "Generating Image Info Json failed.";
        return false;
    }
    m_cachedImagesInfo = json;

    return true;
}

UploadingSession UShareDataProvider::ComposeUploadingSession(const std::vector<int>& shareList)
{
    return m_dataCenter.ComposeUploadingSession(shareList);
}
