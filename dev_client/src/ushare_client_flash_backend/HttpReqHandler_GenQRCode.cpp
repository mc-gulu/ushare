/*!
 * \file HttpReqHandler_GenQRCode.cpp
 * \date 2014/12/06 13:50
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Implementation of HttpReqHandler_GenQRCode
*/

#include "stdafx.h"
#include "HttpReqHandler_GenQRCode.h"

#include "UShareLua51.h"

#include "PlayerDataCenter.h"
#include "UShareDataProvider.h"

#include "HttpSession.h"

#include "FlashHttpProtocol.h"

#include <Poco/URI.h>

void ReplyError(Poco::Net::HTTPServerResponse &resp, const std::string& msg)
{
    resp.setStatus(Poco::Net::HTTPResponse::HTTP_BAD_REQUEST);
    resp.setContentType("text/plain");
    std::ostream& out = resp.send();
    out << msg;
    out.flush();
    LOG(ERROR) << msg;
}

void HttpReqHandler_GenQRCode::handleRequest(Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp)
{
    std::istream& is = req.stream();
    std::string reqBody;
    is >> reqBody;

    std::string decodedURI;
    Poco::URI::decode(reqBody, decodedURI);

    std::vector<int> shareList;
    McUniString shareWords;
    if (!ExtractPlayerSharingListAndWords(decodedURI, shareList, shareWords))
    {
        ReplyError(resp, "Parsing arguments '" + decodedURI + "'(decoded) from flash failed.");
        return;
    }

    LOG(INFO) << "玩家分享文件个数：" << shareList.size();
    LOG(INFO) << "玩家分享文字：" << shareWords.ToAnsi();

    UploadingSession session = UShareDataProvider::Get()->ComposeUploadingSession(shareList);
    if (session.m_sessionID.empty())
    {
        ReplyError(resp, "Invalid uploading session.");
        return;
    }

    // ------------- 上传的业务逻辑从这里开始 -------------

    std::string share_host  = UShareLua51::Get()->GetGlobalString("share_host");
    int share_host_port     = UShareLua51::Get()->GetGlobalInt("share_host_port");
    if (share_host.empty() || share_host_port == LUA_INVALID_INT)
    {
        ReplyError(resp, "Invalid config values (share_host/share_host_port).");
        return;
    }

    std::string share_link_path = UShareLua51::Get()->GetGlobalString("share_link_path");
    if (share_link_path.empty())
    {
        ReplyError(resp, "Invalid config values (share_link_path).");
        return;
    }

    // 建立连接
    HttpSession hs;
    hs.SetHost(share_host, share_host_port);
    if (!hs.QueryTicket(session.m_playerInfo.m_regionName, session.m_playerInfo.m_serverName, session.m_playerInfo.m_accountName, session.m_playerInfo.m_roleName))
    {
        ReplyError(resp, "querying ticket failed.");
        return;
    }
    LOG(INFO) << "ticket retrived successfully. (" << hs.GetTicket() << ")";

    // 校验文件
    std::map<std::string, std::string> UploadingFilesList;
    for (UploadingRecord& record : session.m_selectedRecords)
        UploadingFilesList.insert(std::make_pair("..\\" + record.m_filePath, McUniString(record.m_desc.c_str()).ToUtf8()));

    std::vector<FileInfo> validatedFiles;
    if (!hs.ValidateRequestedFiles(UploadingFilesList, validatedFiles))
    {
        ReplyError(resp, "validating requested filelist failed, file list contains invalid file item.");
        return;
    }

    // 上传文件
    for (FileInfo& fileinfo : validatedFiles)
    {
        if (!hs.UploadFile(fileinfo.m_filePath))
        {
            ReplyError(resp, "Uploading file failed, aborted : File: " + fileinfo.m_filePath);
            return;
        }
    }
    LOG(INFO) << "files uploaded successfully.";

    // 生成分享链接和二维码链接
    if (!hs.GenerateSharedURL(UploadingFilesList, shareWords.ToUtf8()))
    {
        ReplyError(resp, "generating sharing link failed.");
        return;
    }

    // 输出结果
    if (hs.GetShareID().empty())
    {
        ReplyError(resp, "getting empty share_id.");
        return;
    }

    LOG(INFO) << "===== sharing link generated successfully. =====";
    std::string shareLink = tfm::format(share_link_path.c_str(), share_host, share_host_port, hs.GetShareID());
    std::string qrImagePath = tfm::format("%s/%s.jpg", QRCODE_DIR, hs.GetShareID());
    LOG(INFO) << "分享链接: " << shareLink;
    LOG(INFO) << "QR 码文件: " << qrImagePath;

    std::string qrGenArgs = tfm::format(" -o temp/%s -s 5 -l H %s", qrImagePath, shareLink);
    if (!RunProgram("qrcode.exe", (char*)(qrGenArgs.c_str())))
    {
        ReplyError(resp, "Generating qrcode failed.");
        return;
    }

    std::string json = tfm::format("{\"upload_ret\": 0,\
        \"upload_error_msg\": null,\
        \"qrcode_url\": \"/%s\",\
        \"link_url\": \"%s\"}", qrImagePath, shareLink);

    resp.setStatus(Poco::Net::HTTPResponse::HTTP_OK);
    resp.setContentType("application/json");
    std::ostream& out = resp.send();
    out << McUniString(json.c_str()).ToUtf8();
    out.flush();
}

