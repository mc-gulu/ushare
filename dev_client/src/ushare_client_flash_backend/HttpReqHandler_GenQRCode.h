/*!
 * \file HttpReqHandler_GenQRCode.h
 * \date 2014/12/06 13:50
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Definition of HttpReqHandler_GenQRCode 
*/

#pragma once

class HttpReqHandler_GenQRCode : public Poco::Net::HTTPRequestHandler
{
public: 
    virtual void handleRequest(Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp);

protected:

};

