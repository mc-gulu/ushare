/*!
 * \file UShareDataProvider.h
 * \date 2014/12/06 13:57
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Definition of UShareDataProvider 
*/

#pragma once

#include "PlayerDataCenter.h"

class UShareDataProvider 
{
    MC_SINGLETON_DEF(UShareDataProvider);

public:
    bool Init(const char* database);
    std::string GetCachedImagesInfo() const { return m_cachedImagesInfo; }
    UploadingSession ComposeUploadingSession(const std::vector<int>& shareList);

private:
    PlayerDataCenter m_dataCenter;
    std::string m_cachedImagesInfo;
};

