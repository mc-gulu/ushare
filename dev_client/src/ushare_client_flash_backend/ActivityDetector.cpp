/*!
 * \file ActivityDetector.cpp
 * \date 2014/12/08 15:52
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Implementation of ActivityDetector
*/

#include "stdafx.h"
#include "ActivityDetector.h"

#include <cstdio>
#include <windows.h>
#include <tlhelp32.h>

#include "UShareConsts.h"

void ActivityDetector::Run()
{
    if (!m_hTargetProcess)
        return;

    DWORD dwErrCode = WaitForSingleObject(m_hTargetProcess, INFINITE);
    if (dwErrCode == WAIT_OBJECT_0)
    {
        LOG(INFO) << "target process terminated as expected.";
    }
    else
    {
        LOG(INFO) << "Unexpected error occurred while WaitForSingleObject() for the target process.";
        LOG(INFO) << "    Return Code by WaitForSingleObject(): " << dwErrCode;    // refer to the page for explanation http://msdn.microsoft.com/en-us/library/windows/desktop/ms687032(v=vs.85).aspx
        if (dwErrCode == WAIT_FAILED)
        {
            DWORD dwLastError = GetLastError();
            LOG(INFO) << "    Return Code by GetLastError(): " << dwLastError; 
        }
    }

    Poco::Util::ServerApplication::terminate();
}
