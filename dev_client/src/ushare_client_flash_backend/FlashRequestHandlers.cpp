#include "stdafx.h"
#include "FlashRequestHandlers.h"
#include "UShareDataProvider.h"

std::string GLocateFlashTestResource(const std::string& file)
{
    if (file.find("/screenshot/") != std::string::npos)
    {
        return ".." + file;
    }
    if (file.find("/qrcodes/") != std::string::npos)
    {
        return "temp" + file;
    }

    return GFlashTestDataDir + file;
}

void HANDLER_file_serving::handleRequest(Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp)
{
    std::string filepath = GLocateFlashTestResource(req.getURI());

    FILE* f = nullptr;
    errno_t err = fopen_s(&f, filepath.c_str(), "rb");
    bool exists = err == 0 && f;
    if (f)
    {
        fclose(f);
    }

    if (exists)
    {
        resp.setStatus(Poco::Net::HTTPResponse::HTTP_OK);
        resp.sendFile(filepath, "image/jpg");
    } 
    else
    {
        resp.setStatus(Poco::Net::HTTPResponse::HTTP_NOT_FOUND);
    }
}

void HANDLER_GET_images_info::handleRequest(Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp)
{
    McUniString info(UShareDataProvider::Get()->GetCachedImagesInfo().c_str());

    resp.setStatus(Poco::Net::HTTPResponse::HTTP_OK);
    resp.setContentType("application/json");
    std::ostream& out = resp.send();
    out << info.ToUtf8();
    out.flush();
}

void HANDLER_POST_gen_qrcode::handleRequest(Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp)
{
    resp.setStatus(Poco::Net::HTTPResponse::HTTP_OK);
    resp.sendFile(GLocateFlashTestResource("gen_qrcode.json"), "application/json");
}

void HANDLER_GET_status_info::handleRequest(Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp)
{
    resp.setStatus(Poco::Net::HTTPResponse::HTTP_OK);
    resp.sendFile(GLocateFlashTestResource("get_status_info.json"), "application/json");
}

void HANDLER_POST_quit::handleRequest(Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp)
{
    resp.setStatus(Poco::Net::HTTPResponse::HTTP_OK);
    resp.setContentType("text/plain");
    std::ostream& out = resp.send();
    out << "OK";
    out.flush();

    Poco::Util::ServerApplication::terminate();
}
