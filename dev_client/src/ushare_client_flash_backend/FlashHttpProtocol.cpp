/*!
 * \file FlashHttpProtocol.cpp
 * \date 2014/12/08 17:51
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Implementation of FlashHttpProtocol
*/

#include "stdafx.h"
#include "FlashHttpProtocol.h"

#include "UShareConsts.h"

#include <Poco/StringTokenizer.h>
#include <Poco/URI.h>

bool ExtractArg(const std::string& src, const char* argName, std::string& argVal)
{
    size_t argStartPos = src.find(argName);
    if (argStartPos == STR_INVALID_POS)
        return false;

    size_t valStartPos = argStartPos + strlen(argName) + 1;
    if (valStartPos >= src.size())
        return false;

    size_t nextDelimiter = src.find('&', valStartPos);
    if (nextDelimiter == STR_INVALID_POS)
        argVal = src.substr(valStartPos);
    else
        argVal = src.substr(valStartPos, nextDelimiter - valStartPos);

    return true;
}

bool ExtractPlayerSharingListAndWords(const std::string& srcInfo, std::vector<int>& shareList, McUniString& shareWords)
{
    std::string argShareList;
    std::string argShareWords;
    if (!ExtractArg(srcInfo, ARGNAME_SharingList, argShareList))
        return false;
    if (!ExtractArg(srcInfo, ARGNAME_SharingWords, argShareWords))
        return false;

    shareList.clear();
    Poco::StringTokenizer tokenizer(argShareList, ",", Poco::StringTokenizer::TOK_TRIM);
    for (auto it = tokenizer.begin(); it != tokenizer.end(); ++it)
    {
        try
        {
            int val = std::stoi(*it);
            shareList.push_back(val);
        }
        catch (std::exception&)
        {
            continue;
        }
    }
    if (shareList.empty())
        return false;

    shareWords.FromUtf8(argShareWords);
    return true;
}
