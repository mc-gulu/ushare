/*!
 * \file ActivityDetector.h
 * \date 2014/12/08 15:52
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Definition of ActivityDetector 
*/

#pragma once

class ActivityDetector 
{
public: 
    ActivityDetector(HANDLE hProcess) : m_hTargetProcess(hProcess) {}
    ~ActivityDetector() {}

    void Run();

    HANDLE m_hTargetProcess;
};

