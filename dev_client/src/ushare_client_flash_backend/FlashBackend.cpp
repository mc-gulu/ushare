#include "stdafx.h"
#include "FlashBackend.h"

#include "FlashHttpProtocol.h"
#include "FlashRequestHandlers.h"

#include "HttpReqHandler_GenQRCode.h"

int FlashRequestHandler::count = 0;

HTTPRequestHandler* FlashRequestHandlerFactory::createRequestHandler(const HTTPServerRequest & request)
{
    if (request.getMethod() == "GET" && (
        request.getURI().find("/testres/") == 0 ||
        request.getURI().find("/screenshot/") == 0 ||
        request.getURI().find("/qrcodes/") == 0))
    {
        return new HANDLER_file_serving;
    }

    if (request.getMethod() == "GET" && request.getURI() == GET_images_info)
    {
        return new HANDLER_GET_images_info;
    }

    if (request.getMethod() == "POST" && request.getURI() == POST_gen_qrcode)
    {
        return new HttpReqHandler_GenQRCode;
    }

    if (request.getMethod() == "GET" && request.getURI() == GET_status_info)
    {
        return new HANDLER_GET_status_info;
    }

    if (request.getMethod() == "GET" && request.getURI() == POST_quit)
    {
        return new HANDLER_POST_quit;
    }

    return new FlashRequestHandler;
}

int FlashBackendServer::main(const vector<string> &)
{
    HTTPServer s(new FlashRequestHandlerFactory, ServerSocket(SERVER_PORT), new HTTPServerParams);

    s.start();
    cout << endl << "Server started" << endl;

    waitForTerminationRequest();  // wait for CTRL-C or kill

    cout << endl << "Shutting down..." << endl;
    s.stop();

    return Application::EXIT_OK;
}
