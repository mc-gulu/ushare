// ushare_client_flash_backend.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "FileUtility.h"
#include "PlayerDataCenter.h"
#include "FlashBackend.h"

#include "UShareLua51.h"
#include "UShareDataProvider.h"
#include "UShareConsts.h"
#include "ActivityDetector.h"

#include <Poco/RunnableAdapter.h>

_INITIALIZE_EASYLOGGINGPP

int _tmain(int argc, _TCHAR* argv[])
{
    if (!InitEnv(GAppName, GTempDir)) 
    {
        printf("InitEnv() failed. Exiting.");
        return -1;
    }

    UShareLua51::CreateInst();
    if (!UShareLua51::Get()->Init("config.lua"))
    {
        LOG(ERROR) << "UShareLua51 Init() failed.";
        return -1;
    }

    UShareDataProvider::CreateInst();
    if (!UShareDataProvider::Get()->Init(GPlayerSharingDatabase))
    {
        LOG(ERROR) << "UShareDataProvider Init() failed.";
        return -1;
    }

    // 准备工作做好之后启动前端
    HANDLE hProcess = RunProgram(GFlashFrontendProcess);
    if (!hProcess)
    {
        LOG(ERROR) << "Flash frontend starting failed.";
        return -1;
    }

    // 开始监测前端的运行情况
    ActivityDetector detector(hProcess);
    Poco::RunnableAdapter<ActivityDetector> runnable(detector, &ActivityDetector::Run);
    Poco::Thread thread;
    thread.start(runnable);

    // 启动后端
    FlashBackendServer app;
    int ret = app.run(argc, argv);

    UShareDataProvider::DestroyInst();
    UShareLua51::DestroyInst();
    return ret;
}

