/*!
 * \file UShareConsts.h
 * \date 2014/12/08 16:45
 * \author Gu Lu (gulu@kingsoft.com)
 *
 * \brief Definition of UShareConsts 
*/

#pragma once

const std::string::size_type STR_INVALID_POS = std::string::npos;

const int GProcessDetectionInterval = 3000;

const char* const GFlashFrontendProcess = "ushare_ui.exe";

const char* const GAppName = "ushare_client_flash_backend";
const char* const GTempDir = "temp/";
const char* const GPlayerSharingDatabase = "../userdata/share.db";
