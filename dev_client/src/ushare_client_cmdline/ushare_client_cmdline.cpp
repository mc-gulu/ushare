﻿// ushare_client_cmdline.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "upload.h"

#include "HttpSession.h"

#include "FileUtility.h"

#ifdef _DEBUG
#pragma comment (lib, "ushare_client_lib_d")
#else
#pragma comment (lib, "ushare_client_lib")
#endif

#include <ctime>

DEFINE_string(server_name, "", "Server Name");
DEFINE_string(user_name, "", "Account Name");
DEFINE_string(role_name, "", "Role Name");

_INITIALIZE_EASYLOGGINGPP
    
const char* const GAppName = "ushare_client_cmdline";
const char* const GTempDir = "../temp/";

std::string GTestImageFolder = "../test_data/test_images/";

std::string GUploadFileList[] = 
{
    GTestImageFolder + "2014-10-15_11-21-40-000.jpg",
    GTestImageFolder + "2014-11-05_15-28-58-000.jpg",
    GTestImageFolder + "2014-11-07_11-09-55-000.jpg",
    GTestImageFolder + "2014-11-24_19-55-01_os-android.png",
    GTestImageFolder + "2014-11-24_19-55-01_os-ios.png",
    GTestImageFolder + "2014-11-24_19-55-01_os-win.png",
};

std::string GPlayerWords("我的江湖经历~我做主~!");

bool validateCmdlineArgs()
{
    if (FLAGS_server_name.empty())
    {
        LOG(ERROR) << "server name not set.";
        return false;
    }

    if (FLAGS_user_name.empty())
    {
        LOG(ERROR) << "user name not set.";
        return false;
    }

    if (FLAGS_role_name.empty())
    {
        LOG(ERROR) << "role name not set.";
        return false;
    }

    return true;
}

int main(int argc, char* argv[])
{
    if (!InitEnv(GAppName, GTempDir)) 
    {
        printf("InitEnv() failed. Exiting.");
        return -1;
    }

    // parsing commandline arguments
    gflags::ParseCommandLineFlags(&argc, &argv, true);
    if (!validateCmdlineArgs())
    {
        LOG(ERROR) << "commandline validation failed.";
        return -1;
    }


    // ------------- 真正的业务逻辑从这里开始 -------------

    // 建立连接
    HttpSession hs;
    hs.SetHost("localhost", 13080);
    if (!hs.QueryTicket(FLAGS_server_name, FLAGS_user_name, FLAGS_role_name))
    {
        LOG(ERROR) << "querying ticket failed.";
        return -1;
    }
    LOG(INFO) << "ticket retrived successfully. (" << hs.GetTicket() << ")";

    // 校验文件
    std::vector<FileInfo> validatedFiles;
    if (!hs.ValidateRequestedFiles(GUploadFileList, MC_ARRAY_SIZE(GUploadFileList), validatedFiles))
    {
        LOG(ERROR) << "validating requested filelist failed, file list contains invalid file item.";
        return -1;
    }

    // 上传文件
    for (FileInfo& fileinfo : validatedFiles)
    {
        if (!hs.UploadFile(fileinfo.m_filePath))
        {
            LOG(ERROR) << "Uploading file failed, aborted." << std::endl << "File: " << fileinfo.m_filePath;
            return false;
        }
    }
    LOG(INFO) << "files uploaded successfully.";

    // 生成分享链接和二维码链接
    McUniString uniPlayerWords(GPlayerWords.c_str());
    if (!hs.GenerateSharedURL(GUploadFileList, MC_ARRAY_SIZE(GUploadFileList), uniPlayerWords.ToUtf8()))
    {
        LOG(ERROR) << "generating sharing link failed.";
        return -1;
    }

    // 输出结果
    if (!hs.GetShareID().empty())
    {
        LOG(INFO) << "sharing link generated successfully.";
        LOG(INFO) << "sharing link         : " << hs.GetShareID();
        LOG(INFO) << "sharing link (QRCode): " << hs.GetSharedURLQRCode();
    }

    return 0;
}

