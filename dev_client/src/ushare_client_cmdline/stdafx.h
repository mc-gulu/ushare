// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>


#include <gflags/gflags.h>

#include <easylogging++.h>

#include <ShlObj.h>
//#define WIN32_LEAN_AND_MEAN
//#include <windows.h>

#include "../shared_headers.h"
