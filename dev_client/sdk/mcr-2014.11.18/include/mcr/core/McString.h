//=============================================================================
//  Gu Lu, Entire Framework, U Know Games
//
//  Description:
//      McString interface file
//
//  History:
//      2013/6/15 - File Created 
//=============================================================================

#pragma once

#include "mcr/McBuild.h"

#include <string>

/**
    These functions are especially created for simplifying the McUniString interface
    By using these functions we can always know if a conversion succeeds, and we can also 
    ignore the testing when we don't care.

    This is probably an easier way to check the conversion result other than these two choices
        1) 'bool return value'
        2) 'exception'

    for 1) we need to return a bool besides the converted string (which is then returned by parameter or using std::pair/std::tuple )
    for 2) the performance is greatly affected when exception occurs. and the try-catch would mess up code base greatly
    1) is also not applicable when we want to do the conversion in ctor
 */
bool McIsNullString(const std::string& s);
bool McIsNullString(const std::wstring& s);

/**
    McUniString is used for string conversions among encodings

    This class doesn't provide some common string operations like '[]'(indexing) and .find().
    That's because they depends heavily on the underlying encoding format. To simplify the class 
    responsibility, we emphasize that this class acts as a conversion bridge between different 
    encoding types. Each type should then have their own operation sets.
 */
class MCR_API McUniString 
{
public:
    McUniString();
    McUniString(const McUniString& str) : m_dataUtf8(str.m_dataUtf8) {}
    explicit McUniString(const char* ansiString); // 'char*' is considered as common ansi string
    explicit McUniString(const wchar_t* wstr);    // 'wchar_t*' is platform-specific
    virtual ~McUniString() {}

    McUniString& FromUtf8(const std::string& utf8) { m_dataUtf8 = utf8; return *this; }
    std::string ToUtf8() const { return m_dataUtf8; }

    std::string ToAnsi() const;
    std::wstring ToWide() const;

    bool IsNullString() const;
    bool operator == (const McUniString& rhs) const { return m_dataUtf8 == rhs.m_dataUtf8; }

private:
    /**
        UTF-8 is chosen to be the internal representation format of McUniString         
        the pros and cons are listed below:
        
        Pros:
        1) it's uniform and equivalent on all known platforms 
        2) it's relatively compact (comparing to UTF16 & UTF32)

        Cons:
        1) not directly visualized in most debuggers (VS2012 & xcode4)
        2) utf-8 can seldom be directly used, thus usually unnecessary conversions are produced 
    */
    std::string m_dataUtf8;
};

//= EOF =======================================================================