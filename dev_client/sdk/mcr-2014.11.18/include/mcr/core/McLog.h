//=============================================================================
//  Gu Lu, Entire Framework, U Know Games
//
//  Description:
//      McLog.h interface file
//
//  History:
//      2013/7/25 - File Created 
//=============================================================================

#pragma once

void McLog(const char* formatStr, ...);

//= EOF =======================================================================