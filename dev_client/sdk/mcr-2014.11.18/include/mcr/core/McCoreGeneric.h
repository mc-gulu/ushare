//=============================================================================
//  Gu Lu, Entire Framework, U Know Games
//
//  Description:
//      McCoreGeneric interface file
//
//  History:
//      2012/5/26 - File Created 
//=============================================================================

#pragma once

template <typename T> void McDeletePointer(T& ptr)  {if (ptr)    { delete ptr; ptr = NULL; } }
template <typename T> void McDeleteArray(T& pArray) {if (pArray) { delete [] pArray; pArray = NULL; } }

template < typename Container >
void McDeletePtrContainer(Container& cont) 
{
    typename Container::iterator it = cont.begin();
    for (; it != cont.end(); ++it)
    {
        McDeletePointer(*it);
    }

    cont.clear();
}

template < typename Container >
void McDeletePtrMap(Container& cont) 
{
    typename Container::iterator it = cont.begin();
    for (; it != cont.end(); ++it)
    {
        McDeletePointer(it->second);
    }

    cont.clear();
}

//= EOF =======================================================================