//=============================================================================
//  Gu Lu, Entire Framework, U Know Games
//
//  Description:
//      McId.h interface file
//
//  History:
//      2013/7/25 - File Created 
//=============================================================================

#pragma once

#include "mcr/McBuild.h"
#include "McCoreDef.h"

#include <string>

const mcInt64_t MC_INVALID_ID_64 = -1;

class MCR_API McId
{
public:
    union 
    {
        mcInt64_t data64;

        struct
        {
            int i1;
            int i2;
        };

        struct
        {
            char c1;
            char c2;
            char c3;
            char c4;
        };

        struct
        {
            char v[4];
        };
    };

    // ctors and '='
    McId() : data64(MC_INVALID_ID_64) {}
    explicit McId(mcInt64_t data) : data64(data) {}
    McId(const McId& rhs) : data64(rhs.data64) {}
    McId& operator = (const McId& rhs);

    // conversions
    std::string ToString();
    bool FromString(const std::string& str);
    operator mcInt64_t() { return data64; }

    // comparisons 
    bool operator == (McId rhs) { return data64 == rhs.data64; }
    bool operator < (McId rhs) { return data64 < rhs.data64; }
    bool operator < (McId rhs) const { return data64 < rhs.data64; }

    // other 
    bool IsValid() { return data64 != MC_INVALID_ID_64; }
};

//= EOF =======================================================================