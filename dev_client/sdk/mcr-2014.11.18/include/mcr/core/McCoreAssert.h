//=============================================================================
//  Gu Lu, Entire Framework, U Know Games
//
//  Description:
//      McCoreAssert interface file
//
//  History:
//      2012/5/26 - File Created 
//=============================================================================

#pragma once

#include "mcr/McBuild.h"


typedef void (* fnMcAssertionOutput) (const char* content);

class MCR_API McAssertionHandler
{
public:
    static void SetMute(bool val);
    static void SetOutput(fnMcAssertionOutput val);

    bool operator() (
        const char* expr,
        const char* userMsg,
        const char* file,
        const char* func,
        const int line);

private:
    static bool s_mute;
    static fnMcAssertionOutput s_output;
};

#define MC_INVOKE_ASSERT_ROUTINE(exprMsg, userMsg)  { McAssertionHandler h; h(exprMsg, userMsg, __FILE__, __FUNCTION__, __LINE__); }


#if 1 // _DEBUG 
#
#   define MC_ASSERT_IMPL(expr, msg)                { if (!(expr))   MC_INVOKE_ASSERT_ROUTINE(#expr, msg);    }
#   define MC_VERIFY_IMPL(expr, msg)                { if (!(expr))   MC_INVOKE_ASSERT_ROUTINE(#expr, msg);    }
#   define MC_BREAK_IMPL(msg)                       {                MC_INVOKE_ASSERT_ROUTINE("", msg);       }
#
#else
#
#   define MC_ASSERT_IMPL(expr, msg)       
#   define MC_VERIFY_IMPL(expr, msg)       expr
#   define MC_BREAK_IMPL(msg)              
#
#endif  

#define MC_ASSERT(exp)                              MC_ASSERT_IMPL(exp, "")
#define MC_ASSERT_MSG(exp, msg)                     MC_ASSERT_IMPL(exp, msg)

#define MC_BREAK()                                  MC_BREAK_IMPL("")
#define MC_BREAK_MSG(msg)                           MC_BREAK_IMPL(msg)

#define MC_VERIFY(exp)                              MC_VERIFY_IMPL(exp, "")
#define MC_VERIFY_MSG(exp, msg)                     MC_VERIFY_IMPL(exp, msg)

#define MC_ASSERT_RET(op)                           MC_ASSERT(op);              if (!(op)) return;   
#define MC_ASSERT_RET_BOOL(op)                      MC_ASSERT(op);              if (!(op)) return false;
#define MC_ASSERT_RET_PTR(op)                       MC_ASSERT(op);              if (!(op)) return NULL;      
#define MC_ASSERT_RET_CUSTOM(op, badRet)            MC_ASSERT(op);              if (!(op)) return badRet;

#define MC_ASSERT_RET_MSG(op, msg)                  MC_ASSERT_MSG(op, msg);     if (!(op)) { return; }
#define MC_ASSERT_RET_BOOL_MSG(op, msg)             MC_ASSERT_MSG(op, msg);     if (!(op)) { return false; }
#define MC_ASSERT_RET_PTR_MSG(op, msg)              MC_ASSERT_MSG(op, msg);     if (!(op)) { return NULL; }     
#define MC_ASSERT_RET_CUSTOM_MSG(op, badRet, msg)   MC_ASSERT_MSG(op, msg);     if (!(op)) { return badRet; }   


//= EOF =======================================================================