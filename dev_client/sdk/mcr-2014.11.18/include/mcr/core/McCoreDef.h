//=============================================================================
//  Gu Lu, Entire Framework, U Know Games
//
//  Description:
//      McCoreDef.h interface file
//
//  History:
//      2012/5/19 - File Created 
//=============================================================================

#pragma once

#include "mcr/McBuild.h"

const int MC_INVALID_ID = -1;   
const int MC_SHORT_STRING_LEN = 64;
const int MC_STD_STRING_LEN = 4096;

typedef const char* const   McConstStr;

#define MC_ARRAY_OVERRUN_CHECK(i, cnt)          ((i) < (cnt) && (i) >= 0 )

#define MC_ARRAY_SIZE(arr)                      ( sizeof(arr) / sizeof(arr[0]) )

#define MC_EPSILON                              0.00001f

#define MC_ABS(x)                               ((x) < 0 ? -(x) : (x))

#define MC_FUZZY_EQUAL(f1, f2)                  (MC_ABS((f1)-(f2)) < MC_EPSILON) 


#if MCR_WINDOWS
typedef __int64         mcInt64_t;
#else
typedef __int64_t       mcInt64_t;
#endif // MCR_WINDOWS


//= EOF =======================================================================