//=============================================================================
//  Gu Lu, Entire Framework, U Know Games
//
//  Description:
//      McStateMachine interface file
//
//  History:
//      2011/1/26 - File Created 
//=============================================================================

#pragma once

#include "mcr/McBuild.h"
#include <map>
#include <stack>

#define MC_NULL_STATE  (unsigned int)-1

#define MC_DEF_STATE(stateId) \
    virtual unsigned int GetStateId() const { return stateId; } \
    virtual const char* GetStateDesc() const { return #stateId; }

class MCR_API McStateOwner 
{
public:
    virtual ~McStateOwner() {}

    virtual void OnStateEvent(unsigned int curState, unsigned int eventId) {}
};

class MCR_API McState  
{
public:
    McState() : m_owner(NULL) {}
    virtual ~McState() {}

    // state id is a must-have 
    virtual unsigned int GetStateId() const = 0;
    virtual const char* GetStateDesc() const { return ""; }

    // override this method to setup prerequisite which may abort the state-switching process
    virtual bool Prerequisite() { return true; }

    virtual void OnEnter() {}
    virtual void OnLeave() {}
    virtual void Update() {}

    virtual void SetOwner(McStateOwner* val) { m_owner = val; }
    McStateOwner* GetOwner() const { return m_owner; }
                                              
protected:
    McStateOwner* m_owner;
};

class MCR_API McStateNull : public McState 
{
public:
    virtual ~McStateNull() {}

    MC_DEF_STATE(MC_NULL_STATE);
};

class MCR_API McStateMachine 
{
public:
    McStateMachine();
    virtual ~McStateMachine() {}

    virtual unsigned int GetCurState() const;                   
    virtual const char* GetCurStateDesc() const;                   

    virtual void SetOwner(McStateOwner* owner);
    virtual bool SwitchTo(unsigned int stateId);
    virtual void Update();
    virtual void RegisterState(McState* s);

    unsigned int GetRegisteredStateCount() { return m_registeredStates.size(); }

protected:
    McState* m_curState;
    McStateOwner* m_owner;

    typedef std::map<unsigned int, McState*> stateMap_t;
    stateMap_t m_registeredStates; 

public:
    virtual bool PushState(unsigned int newStateId);
    virtual bool PopState();

protected:
    std::stack<McState*> m_stack;
};

//= EOF =======================================================================