//=============================================================================
//  Gu Lu, Entire Framework, U Know Games
//
//  Description:
//      McCoreStrUtil interface file
//
//  History:
//      2012/5/20 - File Created 
//=============================================================================

#pragma once

#include "mcr/McBuild.h"

#include <string>

MCR_API std::string McPrintf(const char* format, ...);

MCR_API std::string McIntToString( const int val );
MCR_API std::string McFloatToString( const float val );
MCR_API bool McStringToInt( const char* src, int& output );
MCR_API bool McStringToFloat( const char* src, float& output );

//= EOF =======================================================================