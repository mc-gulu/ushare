//=============================================================================
//  Gu Lu, Entire Framework, U Know Games
//
//  Description:
//      McCoreClassUtil interface file
//
//  History:
//      2013/3/29 - File Created 
//=============================================================================

#pragma once

#include "mcr/sys/McPIP.h"
#include "mcr/core/McCoreGeneric.h"

//=============================================================================
//  bit-wise copying methods
//=============================================================================
#define MC_DEF_BIT_COPY_METHODS(className) \
    public: \
    className(const className& s)				{ McCrt::MemCpy(this, sizeof(className), &s, sizeof(className)); } \
    className& operator = (const className& s)	{ McCrt::MemCpy(this, sizeof(className), &s, sizeof(className)); return *this; } 

//=============================================================================
//  POD(Plain-Old-Data) object defining
//=============================================================================
#define MC_POD_DEF(className) \
    public: \
    className() { Zero(); } \
    inline void Zero()							{ McCrt::MemSet(this, 0, sizeof(className)); } \
    MC_DEF_BIT_COPY_METHODS(className)

//=============================================================================
//  self & base defining
//=============================================================================
#define MC_POLYM_DEF(derivedClassName, baseClassName) \
    static derivedClassName* Create() { return new derivedClassName; } \
    typedef derivedClassName    self_type; \
    typedef baseClassName       base_type;

//=============================================================================
//  singleton defining
//=============================================================================
/**
 *	Singleton Definition 
 *
 *  Remarks:
 *  1) using CreateInst() & DestroyInst() to enable/disable the singleton
 *  2) using Get() to access the singleton pointer
 *  3) the ctor & dtor are set to be private to prevent being exposed to outside
 */
#define MC_SINGLETON_DEF(className) \
    public: \
    static className* Get() { return MC_SGT_INST(className); } \
    static className* CreateInst() \
        { \
        DestroyInst(); \
        MC_SGT_INST(className) = new className;\
        return MC_SGT_INST(className); \
        } \
        static void DestroyInst() \
        { \
        McDeletePointer(MC_SGT_INST(className)); \
        } \
    private: \
    static className* MC_SGT_INST(className); \
    template <typename T> friend void McDeletePointer(T& ptr); \
    className(); \
    virtual ~className(); // the dtor should be virtual in case it's in a class hierarchy


/**
 *	Singleton Implementation
 */
#define MC_SINGLETON_IMPL(className) \
    className* className::MC_SGT_INST(className) = NULL;

/**
 *	Singleton Object 
 */
#define MC_SGT_INST(className)  s_##className

//= EOF =======================================================================