//=============================================================================
//  Gu Lu, Entire Framework, U Know Games
//
//  Description:
//      McPIP interface file
//
//  History:
//      2013/3/23 - File Created 
//=============================================================================

#pragma once

#include "mcr/sys/McSysDef.h"
#include "mcr/McBuild.h"

class MCR_API McCrt
{
public:
    static void         MemSet(void* dest, int c, size_t countInBytes);
    static McSysErr_e   MemCpy(void* dest, size_t destSizeInBytes, const void* src, size_t countInBytes);
};

/**
    Micro-Carbon Platform Invoking Protocol

    This protocol defines the minimal common platform interface set (MCPIS).
    Functionalities provided by at least two platforms would be considered to 
    move here.
 */
class MCR_API McPIP 
{
public:
    static void DebugBreak();
};

//= EOF =======================================================================