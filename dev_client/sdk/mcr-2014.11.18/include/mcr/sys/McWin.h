//=============================================================================
//  Gu Lu, Entire Framework, U Know Games
//
//  Description:
//      McWin interface file
//
//  History:
//      2013/3/23 - File Created 
//=============================================================================

#pragma once

#include "mcr/McBuild.h"

#if MCR_WINDOWS

MCR_API bool McIsDebuggerAttached();

#endif 

//= EOF =======================================================================
