//=============================================================================
//  Gu Lu, Entire Framework, U Know Games
//
//  Description:
//      McSysDef interface file
//
//  History:
//      2013/3/24 - File Created 
//=============================================================================

#pragma once

enum McSysErr_e
{
    ERR_Ok              = 0,

    ERR_Begin           = 0x80000000, // starts from here to ensure that all error codes are negative value
    ERR_Unknown,
    ERR_End,
};


//= EOF =======================================================================