//=============================================================================
//  Gu Lu, Entire Framework, U Know Games
//
//  Description:
//      McWin interface file
//
//  History:
//      2013/3/23 - File Created 
//=============================================================================

#pragma once

/**
    Platform Identification
 */
#if defined(_WIN32) || defined(_WIN64)
#   define MCR_WINDOWS  1
#endif

/**
    Making sure '_USRDLL' is defined when creating a DLL
 */
#ifdef _USRDLL
#   define MCR_DLL      1
#endif // _USRDLL


/**
    Three linkage-type cases covered in this block:

    1. creating a DLL using this header (MCR_DLL defined)
    2. using a DLL using this header (MCR_USING_DLL defined)
    3. creating or using static library
 */
#if MCR_DLL || MCR_USING_DLL
#   ifdef MCR_EXPORTS
#       define MCR_API __declspec(dllexport)
#   else
#       define MCR_API __declspec(dllimport)
#   endif
#else
#       define MCR_API 
#endif // _USRDLL


/**
    This macro does nothing but act as an indicator for output parameters
 */
#define MCR_OUT_PARAM


#pragma warning ( disable : 4251 ) // warning C4251: 'Foo::m_bar' : class 'Orange' needs to have dll-interface to be used by clients of class 'Foo'


//= EOF =======================================================================
